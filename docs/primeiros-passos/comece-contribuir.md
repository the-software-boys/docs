## 1. Como contribuir

Assim como em todos os projetos Open Source, para fazer uma contribuição é necessário escolher uma issue para ser feita, suprir a tarefa que a issue pede e realizar o pull request ao repositório principal.

Com o SIGE não é diferente. Para fazer uma contribuição ao projeto é necessário seguir algumas regras impostas pelo(s) mantenedor(es) do repositório.

### 1.1 Política de contribuições do SIGE

Para se contribuir com o SIGE é necessário seguir as seguintes regras:

1. Os commits devem estar em português.
2. Os commits devem ser atômicos e significativos, seguindo o padrão especificado pelo [Conventional Commits](conventional-commits.md).
    
    - [Instale os git hooks](instalar-git-hooks.md) de acordo com o projeto.
    
3. O nome da branch em que a alteração será feita deve ter o nome da issue e uma breve descrição a respeito da issue.
4. Quando der assign em uma issue, escreva um comentário dizendo que a mesma está sendo feita.
5. Quando finalizar a issue e fizer o pull request, retorne nos comentários da issue e mencione que existe um pull request aberto para aquela issue.
6. Quando abrir o pull request, mencione (via link) nos comentários do PR a issue que está relacionada a ele.

Após isto basta esperar que a issue seja revisada e aprovada por um mantenedor e pronto, você contribuiu para o projeto.